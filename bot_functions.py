
from urllib.request import *

import json
import re
from time import sleep, time
import requests
import random
import os

import codecs

import config

ptrTitleDiv = re.compile(r'"status".*:.*".*",')
ptrTitle = re.compile(r'".*"')

def mess(message, withoutCD = False):
    config.SOCKET.send(str("PRIVMSG #{} :{}\r\n".format(config.CHAN, message)).encode())
    config.LAST_MESS_TIME = time()


def ban(user):
    mess(".ban {}".format(user))


def timeout(sock, user, seconds = 666):
    mess(".timeout {}".format(user, seconds))


#req = request
#res = response
def fillOpList():
    while True:
        try:
            url = config.MY_CHAT_URL
            req = Request(url, headers={"accept": "*/*"})
            res = urlopen(req).read()
            if res.find("502 bad gateway") == - 1:
                config.oplist.clear()
                data = json.loads(res)
                for p in data["chatters"]["moderators"]:
                    config.oplist[p] = "mod"
                for p in data["chatters"]["global_mods"]:
                    config.oplist[p] = "global_mod"
                for p in data["chatters"]["admins"]:
                    config.oplist[p] = "admin"
                for p in data["chatters"]["staff"]:
                    config.oplist[p] = "staff"
        except:
            log = open("log.txt", "w+")
            log.write("\nWe have some trouble: ")
        sleep(5)

def fillUniverse(universeName):
    universeFileName = config.UNIVERSE_DIR + "/" + universeName + ".txt"
    universeFile = codecs.open(universeFileName, "r", "utf_8_sig")
    universeFileText = universeFile.read()
    projectDict = json.loads(universeFileText)
    config.ALL_UNIVERSE_STORIES[universeName] = []
    for story in projectDict:
        story["time"] = None
        config.ALL_UNIVERSE_STORIES[universeName].append(story)

    universeFile.close()
    pass

def fillUniverseList():
    for walkNames in os.walk(config.UNIVERSE_DIR):
        print(walkNames)
        for fileName in walkNames[2]:
            uniName = fileName[:-4]
            config.ALL_UNIVERSE_LIST.append(uniName)
            print(uniName)
            fillUniverse(uniName)

def isAdmin(user):
    return user in config.oplist

# curl -H "Accept: application/vnd.twitchtv.v5+json"
# -H "Client-ID: 06g1g0vujakq4vmsfmncelztzce0ec"
# -X GET "https://api.twitch.tv/kraken/streams/173881372"

def getStreamTitle():
    headers = {
        'accept': 'application/vnd.twitchtv.v5+json',
        'client-id': '06g1g0vujakq4vmsfmncelztzce0ec'
    }
    r = requests.get("https://api.twitch.tv/kraken/streams/173881372", headers=headers)

    html = str(r.text.encode('cp1251'))
    print(html)
    if config.IS_DEBUG_MOD:
        html = '{"mature": true, "status": "DS-3. fasdasdasd", "bro'

    if '{"stream":null}' in html:
        return None

    for line in html.split("\n"):
        titleDiv = ptrTitleDiv.search(line)
        if titleDiv:
            fullTitle = titleDiv.group(0).split(":")[1].strip()
            title = ptrTitle.search(fullTitle).group(0).strip()[1:-1]
            return title

    return None

def getUniverseName():
    if config.CUR_GAME_UNIVERSE:
        return config.CUR_GAME_UNIVERSE

    title = getStreamTitle()

    if title == None:
        return None

    print(config.ALL_UNIVERSE_LIST)
    for universe in config.ALL_UNIVERSE_LIST:
        if universe in title:
            config.CUR_GAME_UNIVERSE = universe
            return universe

    return None

def timeTo(lastTime, cd):
    if lastTime == None:
        return True
    curTime = time()
    if curTime - lastTime > cd:
        return True
    return False

def timeToStory(story):
    storyTime = story["time"]
    if storyTime == None:
        return True
    if timeTo(storyTime, config.STORY_CD):
        return True
    return False

def lor(username):
    universe = getUniverseName()
    print(universe)
    if universe == None:
        return

    universeStories = config.ALL_UNIVERSE_STORIES[universe]
    curTimeStories = [ story for story in universeStories if timeToStory(story) ]

    catMess = "@" + username + " "
    if len(curTimeStories) == 0:
        catMess += config.DEFAULT_STORY
        mess(catMess)
        return

    story = random.choice(curTimeStories)

    story["time"] = time()
    storyText = story["text"]

    catMess += storyText
    mess(catMess)
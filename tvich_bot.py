from ...botConfigs import config
import bot_functions
import socket
import re
import time
from _thread import start_new_thread
from time import sleep



def main():
    config.SOCKET = socket.socket()
    config.SOCKET.connect((config.HOST, config.PORT))
    config.SOCKET.send("PASS {}\r\n".format(config.PASS).encode("utf-8"))
    config.SOCKET.send("NICK {}\r\n".format(config.NICK).encode("utf-8"))
    config.SOCKET.send("JOIN #{}\r\n".format(config.CHAN).encode("utf-8"))

    chat_message = re.compile(r"^:\w+!\w+@\w+\.tmi\.twitch\.tv PRIVMSG #\w+ :")
    bot_functions.mess("Мррррр мяу...")

    bot_functions.fillUniverseList()
    start_new_thread(bot_functions.fillOpList, ())
    while True:
        response = config.SOCKET.recv(1024).decode("utf-8")
        if response == "PING :tmi.twitch.tv\r\n":
            config.SOCKET.send("POND :tmi.twitch.tv\r\n".encode("utf-8"))
        else:
            username = re.search(r"\w+", response).group(0)
            message = chat_message.sub("", response)
            print(response)

            if "!лор" in message:
                bot_functions.lor(username)


        sleep(1)





if __name__ == "__main__":
    main()